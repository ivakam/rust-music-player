mod util;
mod backend;
mod components;

use crate::components::player::Body;
use dioxus::prelude::*;
use dioxus_desktop::tao::dpi::PhysicalSize;
use dioxus_desktop::WindowBuilder;
use log::{error, info};
use rodio::{OutputStream, Sink};
use std::process;
use crate::backend::io_utils;
use crate::backend::structs::{Album, AppConfig, ID, Track};
use crate::components::newtypes::{AlbumGroupTree, MainSink};
use crate::components::props::AppProps;

static DEFAULT_COVER: &str = "/assets/placeholder.png";

fn main() {
    env_logger::init();
    let (_stream, stream_handle) = match OutputStream::try_default() {
        Ok((stream, handle)) => (stream, handle),
        Err(err) => {
            error!("Could not open an output stream: {}", err);
            process::exit(1);
        }
    };
    let main_sink = match Sink::try_new(&stream_handle) {
        Ok(sink) => sink,
        Err(err) => {
            error!("Couldn't open device: {}", err);
            process::exit(1);
        }
    };
    let app_props = AppProps {
        sink: MainSink::new(main_sink),
    };

    dioxus_desktop::launch_with_props(app, app_props, get_window_cfg());
}

fn app(cx: Scope<AppProps>) -> Element {
    let cfg = match io_utils::load_config() {
        Ok(cfg) => cfg,
        Err(err) => {
            error!("Could not initialize a valid configuration: {}", err);
            process::exit(1);
        }
    };

    cx.use_hook(|| {
        cx.provide_context(cx.props.sink.clone());
        cx.provide_context(cfg);
    });

    info!("Building body...");
    let body = rsx! {
        Body { }
    };

    info!("Performing final render...");
    cx.render(rsx! {
        style { [include_str!("../src/style.css")] }
        body
    })
}

fn get_window_cfg() -> dioxus_desktop::Config {
    let w = WindowBuilder::new();
    let w = w.with_title("Musee");
    let w = w.with_inner_size(PhysicalSize::new(1080, 700));
    let c = dioxus_desktop::Config::new();
    c.with_window(w)
}
