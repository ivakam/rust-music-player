use crate::backend::track_player::TrackPlayer;
//use crate::backend::structs::Dictionary;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::os::raw::c_double;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use zbus::{dbus_interface, Connection, ConnectionBuilder, Result, SignalContext};
use zvariant::{OwnedValue};

#[derive(Clone, Copy, Serialize, Deserialize, Debug)]
enum PlaybackStatus {
    Playing,
    Paused,
    Stopped,
}

impl Display for PlaybackStatus {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write! {f, "{:?}", self}
    }
}

#[derive(Clone, Copy, Serialize, Deserialize, Debug)]
enum LoopStatus {
    None,
    Track,
    Playlist,
}

impl Display for LoopStatus {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write! {f, "{:?}", self}
    }
}

pub struct MuseeMPRIS {
    player: Arc<Mutex<TrackPlayer>>,
    connection: Option<Connection>,
}

impl MuseeMPRIS {
    pub fn new(player: Arc<Mutex<TrackPlayer>>) -> Self {
        MuseeMPRIS {
            player,
            connection: None,
        }
    }

    pub fn init_dbus_connection(mut self) {
        tokio::spawn(async move {
            let b_int = MPRISBase::new();
            let p_int = MPRISPlayer::new(self.player.clone());
            self.connection = Some(
                ConnectionBuilder::session()
                    .expect("Failed to get dbus session")
                    .name("org.mpris.MediaPlayer2.musee")
                    .expect("Failed to acquire dbus object name")
                    .serve_at("/org/mpris/MediaPlayer2", b_int)
                    .expect("Failed to initialize dbus object for base interface")
                    .serve_at("/org/mpris/MediaPlayer2", p_int)
                    .expect("Failed to initialize dbus object for Player interface")
                    .build()
                    .await
                    .unwrap(),
            );

            let t_cb = self.on_track_changed();
            TrackPlayer::add_track_change_listener(self.player.clone(), t_cb);

            let ps_cb = self.on_play_status_changed();
            TrackPlayer::add_play_status_change_listener(self.player.clone(), ps_cb);

            loop {}
        });
    }

    // TODO: Look over cloning weirdness
    fn on_track_changed(&self) -> Arc<dyn Fn() + Send + Sync + 'static> {
        let (p1, p2) = (self.player.clone(), self.player.clone());
        let conn = self.connection.clone().unwrap();
        Arc::new(move || {
            let c = TrackPlayer::current(p1.clone());
            let track = TrackPlayer::get_track_from_id(p2.clone(), c);

            let mdata = track.to_mpris();
            let mdata = mdata
                .iter()
                .map(|(k, v)| (k.clone(), OwnedValue::from(v)))
                .collect();
            let conn = conn.clone();
            tokio::spawn(async move {
                // Get the InterfaceRef for the MPRISPlayer
                let p_ref = conn
                    .object_server()
                    .interface::<_, MPRISPlayer>("/org/mpris/MediaPlayer2")
                    .await
                    .expect("Failed to await p_ref");

                // Change the property and send the changed signal
                {
                    let mut p_mut = p_ref.get_mut().await;
                    p_mut.metadata = mdata;
                }
                p_ref
                    .get()
                    .await
                    .metadata_changed(p_ref.signal_context())
                    .await
                    .expect("Failed to notify change");
            });
        })
    }

    fn on_play_status_changed(&self) -> Arc<dyn Fn() + Send + Sync + 'static> {
        let p1 = self.player.clone();
        let conn = self.connection.clone().unwrap();
        Arc::new(move || {
            let mut play_status = PlaybackStatus::Stopped;
            match TrackPlayer::is_paused(p1.clone()) {
                true => play_status = PlaybackStatus::Paused,
                false => play_status = PlaybackStatus::Playing,
            }

            let conn = conn.clone();
            tokio::spawn(async move {
                // Get the InterfaceRef for the MPRISPlayer
                let p_ref = conn
                    .object_server()
                    .interface::<_, MPRISPlayer>("/org/mpris/MediaPlayer2")
                    .await
                    .expect("Failed to await p_ref");

                // Change the property and send the changed signal
                {
                    let mut p_mut = p_ref.get_mut().await;
                    p_mut.playback_status = play_status;
                }
                p_ref
                    .get()
                    .await
                    .playback_status_changed(p_ref.signal_context())
                    .await
                    .expect("Failed to notify change");
            });
        })
    }
}

struct MPRISBase {
    /* Fields for MediaPlayer2 interface */
    can_quit: bool,
    fullscreen: bool,
    can_set_fullscreen: bool,
    can_raise: bool,
    has_track_list: bool,
    identity: String,
    desktop_entry: String,
    supported_uri_schemes: Vec<String>,
    supported_mime_types: Vec<String>,
}

impl MPRISBase {
    pub fn new() -> Self {
        MPRISBase {
            can_quit: true,
            fullscreen: false,
            can_set_fullscreen: false,
            can_raise: true,
            has_track_list: false,
            identity: "Musee".to_string(),
            desktop_entry: "".to_string(),
            supported_uri_schemes: vec!["file".to_string()],
            supported_mime_types: vec!["audio/mpeg".to_string()],
        }
    }
}

#[dbus_interface(name = "org.mpris.MediaPlayer2")]
impl MPRISBase {
    /**************************
     * MediaPlayer2 Interface *
     **************************/

    /* Methods */

    async fn raise(&self) {
        println!("Raise not implemented");
    }

    async fn quit(&self) {
        println!("Quit not implemented");
    }

    /* Properties */

    #[dbus_interface(property)]
    async fn can_quit(&self) -> bool {
        self.can_quit
    }

    #[dbus_interface(property)]
    async fn fullscreen(&self) -> bool {
        self.fullscreen
    }

    #[dbus_interface(property)]
    async fn can_set_fullscreen(&self) -> bool {
        self.can_set_fullscreen
    }

    #[dbus_interface(property)]
    async fn can_raise(&self) -> bool {
        self.can_raise
    }

    #[dbus_interface(property)]
    async fn has_track_list(&self) -> bool {
        self.has_track_list
    }

    #[dbus_interface(property)]
    async fn identity(&self) -> String {
        self.identity.clone()
    }

    #[dbus_interface(property)]
    async fn desktop_entry(&self) -> String {
        self.desktop_entry.clone()
    }

    #[dbus_interface(property)]
    async fn supported_uri_schemes(&self) -> Vec<String> {
        self.supported_uri_schemes.clone()
    }

    #[dbus_interface(property)]
    async fn supported_mime_types(&self) -> Vec<String> {
        self.supported_mime_types.clone()
    }
}

struct MPRISPlayer {
    player: Arc<Mutex<TrackPlayer>>,
    /* Fields for MediaPlayer2.Player interface */
    playback_status: PlaybackStatus,
    loop_status: LoopStatus,
    rate: c_double,
    shuffle: bool,
    metadata: HashMap<String, OwnedValue>,
    volume: c_double,
    position: Duration,
    minimum_rate: c_double,
    maximum_rate: c_double,
    can_go_next: bool,
    can_go_previous: bool,
    can_play: bool,
    can_pause: bool,
    can_seek: bool,
    can_control: bool,
}

impl MPRISPlayer {
    pub fn new(player: Arc<Mutex<TrackPlayer>>) -> Self {
        MPRISPlayer {
            player,
            playback_status: PlaybackStatus::Stopped,
            loop_status: LoopStatus::None,
            rate: 1.0,
            shuffle: false,
            metadata: HashMap::new(),
            /*
            LEAVE BE, This is the better way to do this, but is currently impossible due to bugs in zbus.
             */
            //metadata: Dictionary {
            //    f1: "No title".to_string(),
            //    f2: vec!("No artist".to_string()),
            //    f3: "No album".to_string()
            //},
            volume: 1.0,
            position: Duration::from_secs(0),
            minimum_rate: 1.0,
            maximum_rate: 1.0,
            can_go_next: true,
            can_go_previous: true,
            can_play: true,
            can_pause: true,
            can_seek: false,
            can_control: true,
        }
    }
}

#[dbus_interface(name = "org.mpris.MediaPlayer2.Player")]
impl MPRISPlayer {
    /*********************************
     * MediaPlayer2.Player Interface *
     *********************************/

    /* Methods */

    async fn next(&self) {
        TrackPlayer::next(self.player.clone());
    }

    async fn previous(&self) {
        TrackPlayer::prev(self.player.clone());
    }

    async fn pause(&self) {
        TrackPlayer::pause(self.player.clone());
    }

    async fn play_pause(&self) {
        TrackPlayer::toggle_pause(self.player.clone());
    }

    async fn stop(&self) {
        println!("Stop not implemented");
    }

    async fn play(&self) {
        TrackPlayer::play(self.player.clone());
    }

    async fn seek(&self, offset: i64) {
        println!("Seek not implemented");
    }

    async fn set_position(&self, path: String, position: i64) {
        println!("SetPosition not implemented");
    }

    async fn open_uri(&self, uri: String) {
        println!("OpenUri not implemented");
    }

    /* Properties */

    #[dbus_interface(property)]
    async fn playback_status(&self) -> String {
        self.playback_status.to_string()
    }

    #[dbus_interface(property)]
    async fn loop_status(&self) -> String {
        self.loop_status.to_string()
    }

    #[dbus_interface(property)]
    async fn rate(&self) -> c_double {
        self.rate
    }

    #[dbus_interface(property)]
    async fn shuffle(&self) -> bool {
        self.shuffle
    }

    #[dbus_interface(property)]
    async fn metadata(&self) -> HashMap<String, OwnedValue> {
        self.metadata.clone()
    }

    #[dbus_interface(property)]
    async fn volume(&self) -> c_double {
        self.volume
    }

    #[dbus_interface(property)]
    async fn position(&self) -> i64 {
        self.position.as_micros() as i64
    }

    #[dbus_interface(property)]
    async fn minimum_rate(&self) -> c_double {
        self.minimum_rate
    }

    #[dbus_interface(property)]
    async fn maximum_rate(&self) -> c_double {
        self.maximum_rate
    }

    #[dbus_interface(property)]
    async fn can_go_next(&self) -> bool {
        self.can_go_next
    }

    #[dbus_interface(property)]
    async fn can_go_previous(&self) -> bool {
        self.can_go_previous
    }

    #[dbus_interface(property)]
    async fn can_play(&self) -> bool {
        self.can_play
    }

    #[dbus_interface(property)]
    async fn can_pause(&self) -> bool {
        self.can_pause
    }

    #[dbus_interface(property)]
    async fn can_seek(&self) -> bool {
        self.can_seek
    }

    #[dbus_interface(property)]
    async fn can_control(&self) -> bool {
        self.can_control
    }

    /* Signals */
    #[dbus_interface(signal)]
    async fn seeked(ctxt: &SignalContext<'_>) -> Result<()>;
}
