use anyhow::{Context, Result};
use core::time::Duration;
use log::{error, info};
use rodio::Source;
use std::collections::VecDeque;
use std::fs::File;
use std::path::Path;
use symphonia::core::audio::SampleBuffer;
use symphonia::core::codecs::CODEC_TYPE_NULL;
use symphonia::core::codecs::{Decoder, DecoderOptions};
use symphonia::core::formats::{FormatOptions, SeekMode, SeekTo};
use symphonia::core::formats::{FormatReader, Track};
use symphonia::core::io::MediaSourceStream;
use symphonia::core::meta::MetadataOptions;
use symphonia::core::probe::Hint;
use symphonia::core::units::Time;

pub struct Skippable {
    new_samples: SampleBuffer<f32>,
    sample_buf: VecDeque<f32>,
    reader: Box<dyn FormatReader>,
    decoder: Box<dyn Decoder>,
    channels: u16,
    sample_rate: u32,
    total_duration: Option<Duration>,
}

impl Skippable {
    pub fn try_create(file_name: String, start_at: Option<f64>) -> Result<Self> {
        // Create a hint to help the format registry guess what format reader is appropriate.
        let mut hint = Hint::new();

        let path = Path::new(&file_name);

        // Provide the file extension as a hint.
        if let Some(extension) = path.extension() {
            if let Some(extension_str) = extension.to_str() {
                hint.with_extension(extension_str);
            }
        }

        let source = Box::new(File::open(&file_name).context("Failed to open file")?);

        // Create the media source stream using the boxed media source from above.
        let mss = MediaSourceStream::new(source, Default::default());

        // Use the default options for format readers other than for gapless playback.
        let format_opts: FormatOptions = Default::default();

        // Use the default options for metadata readers.
        let metadata_opts: MetadataOptions = Default::default();

        // Set the decoder options.
        let decode_opts: DecoderOptions = Default::default();

        // Probe the media source stream for metadata and get the format reader.
        let mut reader = symphonia::default::get_probe()
            .format(&hint, mss, &format_opts, &metadata_opts)
            .context("Couldn't probe file")?
            .format;

        let track = first_supported_track(reader.tracks()).expect("No supported tracks");
        let track_id = track.id;

        let total_duration: Option<Duration> = if let Some(tb) = track.codec_params.time_base {
            if let Some(n_frames) = track.codec_params.n_frames {
                let time = tb.calc_time(n_frames);
                Some(Duration::from_secs_f64(time.seconds as f64 + time.frac))
            } else {
                None
            }
        } else {
            None
        };

        let mut decoder = symphonia::default::get_codecs()
            .make(&track.codec_params, &decode_opts)
            .context("Failed to create decoder")?;

        if let Some(t) = start_at {
            let seek_to = SeekTo::Time {
                time: Time::from(t),
                track_id: Some(track_id),
            };
            match reader.seek(SeekMode::Accurate, seek_to) {
                Ok(_) => (),
                Err(_) => {
                    error!("Seek failed for file: {}", file_name);
                }
            }
        }

        let first_packet = reader.next_packet().context("Failed to get first packet")?;

        if first_packet.track_id() != track_id {
            error!("First packet track ID doesn't match the selected track ID");
        }

        let decoded = decoder
            .decode(&first_packet)
            .context("Failed to decode first packet, can't get metadata about the provided file")?;
        let spec = *decoded.spec();
        let duration = decoded.capacity() as u64;
        let mut new_samples = SampleBuffer::<f32>::new(duration, spec);
        new_samples.copy_interleaved_ref(decoded);

        let mut sample_buf = VecDeque::new();
        sample_buf.extend(new_samples.samples());
        info!("Successfully initialized source for {}", file_name);
        Ok(Self {
            new_samples,
            sample_buf,
            reader,
            decoder,
            channels: spec.channels.count() as u16, // FIXME: is always safe?
            sample_rate: spec.rate,
            total_duration,
        })
    }
}

impl Iterator for Skippable {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        let sample = match self.sample_buf.pop_front() {
            Some(s) => s,
            None => {
                loop {
                    let packet = match self.reader.next_packet() {
                        Ok(packet) => packet,
                        _ => return None, // EOF
                    };
                    match self.decoder.decode(&packet) {
                        Ok(decoded) => {
                            self.new_samples.copy_interleaved_ref(decoded);
                            self.sample_buf.extend(self.new_samples.samples());
                            // Safe unwrap since we just extended sample_buf
                            break self.sample_buf.pop_front().unwrap();
                        }
                        // Decode errors are recoverable, continue to next packet
                        Err(err) => {
                            error!("Decode error: {}", err);
                            continue;
                        }
                    };
                }
            }
        };
        Some(sample)
    }
}

impl Source for Skippable {
    fn current_frame_len(&self) -> Option<usize> {
        Some(self.sample_buf.len())
    }

    fn channels(&self) -> u16 {
        self.channels
    }

    fn sample_rate(&self) -> u32 {
        self.sample_rate
    }

    fn total_duration(&self) -> Option<Duration> {
        self.total_duration
    }
}

fn first_supported_track(tracks: &[Track]) -> Option<&Track> {
    tracks
        .iter()
        .find(|t| t.codec_params.codec != CODEC_TYPE_NULL)
}
