use crate::{Track, ID, DEFAULT_COVER, Album, io_utils, AppConfig, AlbumGroupTree};
use anyhow::{bail, Result};
use log::{error};
use rodio::{Sink};
use std::collections::HashMap;
use std::ops::AddAssign;
use std::process;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use tokio::sync::watch;
use tokio::task::JoinHandle;
use tokio::time;
use crate::backend::dbus::MuseeMPRIS;
use crate::backend::skippable::Skippable;
use crate::backend::structs::SortOpt;
use crate::backend::structs::SortOpt::Asc;

static ELAPSED_POLL_RATE: u64 = 50;

pub struct TrackPlayer {
    track_map: Arc<HashMap<ID, Track>>,
    album_map: Arc<HashMap<ID, Album>>,
    group_tree: AlbumGroupTree,
    tracks: Vec<ID>,
    sink: Arc<Sink>,
    current: usize,
    pub duration: Duration,
    pub elapsed: Duration,
    pub repeat: bool,
    volume: f32,
    listener: Option<JoinHandle<()>>,
    timer: Option<JoinHandle<()>>,
    track_change_listeners: Vec<Arc<dyn Fn() + Send + Sync + 'static>>,
    elapsed_change_listeners: Vec<Arc<dyn Fn() + Send + Sync + 'static>>,
    volume_change_listeners: Vec<Arc<dyn Fn() + Send + Sync + 'static>>,
    play_status_change_listeners: Vec<Arc<dyn Fn() + Send + Sync + 'static>>,
}

impl TrackPlayer {
    pub fn new(cfg: &mut AppConfig, sink: Arc<Sink>) -> TrackPlayer {
        let (album_map,
            track_map,
            group_tree) = io_utils::scan_for_albums(cfg).expect("Failed to scan for albums");
        TrackPlayer {
            track_map: Arc::new(track_map),
            album_map: Arc::new(album_map),
            group_tree,
            tracks: Vec::new(),
            sink,
            current: 0,
            duration: Duration::from_secs(0),
            elapsed: Duration::from_secs(0),
            repeat: true,
            volume: 1.0,
            listener: None,
            timer: None,
            track_change_listeners: Vec::new(),
            elapsed_change_listeners: Vec::new(),
            volume_change_listeners: Vec::new(),
            play_status_change_listeners: Vec::new(),
        }
    }

    pub fn setup_tracks(this: Arc<Mutex<Self>>, selection: &Vec<String>) -> Vec<(ID, Vec<ID>)> {
        let t = this.lock().unwrap();
        let grouped_albums = t.group_tree.clone();
        let selected_group = grouped_albums.follow_path(selection);
        let sorts: Vec<SortOpt> = vec![
            Asc("title".to_string()),
            Asc("artist".to_string()),
            Asc("year".to_string()),
        ];
        let albums_to_show: Vec<ID> =
            selected_group.to_sorted_vec(t.album_map.clone(), sorts);

        let mut albums_with_sorted_tracks: Vec<(ID, Vec<ID>)> = Vec::new();
        for a in albums_to_show {
            let ts = t.album_map[&a].get_sorted_tracks(t.track_map.clone());
            albums_with_sorted_tracks.push((a, ts))
        }
        drop(t);

        if !TrackPlayer::is_active(this.clone()) {
            for (_, ts) in &albums_with_sorted_tracks {
                TrackPlayer::add_tracks(this.clone(), ts.clone());
            }
        }

        albums_with_sorted_tracks
    }

    pub fn init_dbus_connection(this: Arc<Mutex<Self>>) {
        let dbus = MuseeMPRIS::new(this.clone());
        dbus.init_dbus_connection();
    }

    pub fn add_track_change_listener(
        this: Arc<Mutex<Self>>,
        closure: Arc<dyn Fn() + Send + Sync + 'static>,
    ) {
        let mut t = this.lock().unwrap();
        t.track_change_listeners.push(closure);
    }

    pub fn add_elapsed_change_listener(
        this: Arc<Mutex<Self>>,
        closure: Arc<dyn Fn() + Send + Sync + 'static>,
    ) {
        let mut t = this.lock().unwrap();
        t.elapsed_change_listeners.push(closure);
    }

    pub fn add_volume_change_listener(
        this: Arc<Mutex<Self>>,
        closure: Arc<dyn Fn() + Send + Sync + 'static>,
    ) {
        let mut t = this.lock().unwrap();
        t.volume_change_listeners.push(closure);
    }

    pub fn add_play_status_change_listener(
        this: Arc<Mutex<Self>>,
        closure: Arc<dyn Fn() + Send + Sync + 'static>,
    ) {
        let mut t = this.lock().unwrap();
        t.play_status_change_listeners.push(closure);
    }

    pub fn add_tracks(this: Arc<Mutex<Self>>, mut ids: Vec<ID>) -> usize {
        let mut t = this.lock().unwrap();
        t.tracks.append(&mut ids);
        t.tracks.len() - 1
    }

    pub fn add_track(this: Arc<Mutex<Self>>, id: ID) -> usize {
        let mut t = this.lock().unwrap();
        t.tracks.push(id);
        t.tracks.len() - 1
    }

    pub fn clear_tracks(this: Arc<Mutex<Self>>) {
        let mut t = this.lock().unwrap();
        t.tracks.clear();
    }

    pub fn group_tree(this: Arc<Mutex<Self>>) -> AlbumGroupTree {
        let t = this.lock().unwrap();
        t.group_tree.clone()
    }

    pub fn current(this: Arc<Mutex<Self>>) -> ID {
        let t = this.lock().unwrap();
        t.tracks[t.current]
    }

    pub fn get_track_from_id(this: Arc<Mutex<Self>>, track: ID) -> Track {
        let t = this.lock().unwrap();
        t.track_map[&track].clone()
    }

    pub fn get_album_from_id(this: Arc<Mutex<Self>>, album: ID) -> Album {
        let t = this.lock().unwrap();
        t.album_map[&album].clone()
    }

    pub fn get_tracks_for_album(this: Arc<Mutex<Self>>, album: ID) -> Vec<ID> {
        let t = this.lock().unwrap();
        t.album_map[&album].get_sorted_tracks(t.track_map.clone())
    }

    pub fn get_current_cover(this: Arc<Mutex<Self>>) -> String {
        let mut cover = DEFAULT_COVER.to_string();
        if TrackPlayer::is_active(this.clone()) {
            let c = TrackPlayer::current(this.clone());
            let c_track = TrackPlayer::get_track_from_id(this.clone(), c);
            let t = this.lock().unwrap();
            cover = t.album_map[&c_track.album_id].cover.clone();
        }
        cover
    }

    pub fn volume(this: Arc<Mutex<Self>>) -> f32 {
        let t = this.lock().unwrap();
        t.volume
    }

    pub fn set_volume(this: Arc<Mutex<Self>>, vol: f32) {
        let mut t = this.lock().unwrap();
        t.volume = vol;
        t.sink.set_volume(vol);
        let listeners = t.volume_change_listeners.clone();
        drop(t);
        for c in listeners {
            c();
        }
    }

    pub fn duration(this: Arc<Mutex<Self>>) -> Duration {
        let t = this.lock().unwrap();
        t.duration
    }

    pub fn elapsed(this: Arc<Mutex<Self>>) -> Duration {
        let t = this.lock().unwrap();
        t.elapsed
    }

    pub fn is_paused(this: Arc<Mutex<Self>>) -> bool {
        let t = this.lock().unwrap();
        t.sink.is_paused()
    }

    pub fn is_active(this: Arc<Mutex<Self>>) -> bool {
        let t = this.lock().unwrap();
        !t.tracks.is_empty()
    }

    pub fn next(this: Arc<Mutex<Self>>) {
        let mut t = this.lock().unwrap();
        if t.repeat {
            t.current = (t.current + 1) % (t.tracks.len());
        }
        drop(t);
        let c = TrackPlayer::current(this.clone());
        TrackPlayer::play_track(this, c, Duration::from_secs(0));
    }

    pub fn prev(this: Arc<Mutex<Self>>) {
        let mut t = this.lock().unwrap();
        if t.repeat {
            if t.current == 0 {
                t.current = t.tracks.len() - 1;
            } else {
                t.current -= 1;
            }
        }
        drop(t);
        let c = TrackPlayer::current(this.clone());
        TrackPlayer::play_track(this, c, Duration::from_secs(0));
    }

    pub fn find_and_play(this: Arc<Mutex<Self>>, id: ID) {
        let mut t = this.lock().unwrap();
        t.current = match t.tracks.iter().position(|i| *i == id) {
            Some(pos) => pos,
            None => {
                error!("Could not find track with ID {}", id);
                process::exit(1);
            }
        };
        drop(t);
        let c = TrackPlayer::current(this.clone());
        TrackPlayer::play_track(this, c, Duration::from_secs(0));
    }

    pub fn play_track(this: Arc<Mutex<Self>>, id: ID, start_time: Duration) -> Result<()> {
        let mut t = this.lock().unwrap();
        if t.sink.len() > 0 {
            t.sink.clear();
        }
        t.elapsed = start_time;
        let path;
        (t.duration, path) = if let Some(tr) = t.track_map.get(&id) {
            (tr.duration, tr.file_path.to_owned())
        } else {
            // TODO: Handle invalid track ID
            bail!("Invalid track ID: {}", id);
        };

        let source = Skippable::try_create(path, Some(start_time.as_secs() as f64))?;
        t.sink.append(source);

        let listeners = t.track_change_listeners.clone();
        drop(t);
        // Notify all listeners that the current track has changed
        for c in listeners {
            c();
        }

        TrackPlayer::play(this.clone());

        let (tx, mut rx) = watch::channel(Duration::from_millis(0));

        let mut t = this.lock().unwrap();

        if let Some(l) = &t.listener {
            l.abort();
            t.listener = None;
        }

        if let Some(tim) = &t.timer {
            tim.abort();
            t.timer = None;
        }

        let l_this = this.clone();

        t.listener = Some(tokio::spawn(async move {
            while rx.changed().await.is_ok() {
                if !TrackPlayer::is_paused(l_this.clone()) {
                    TrackPlayer::incr_elapsed(l_this.clone(), *rx.borrow());
                    let t = l_this.lock().unwrap();
                    let listeners = t.elapsed_change_listeners.clone();
                    if t.elapsed >= t.duration {
                        drop(t);
                        TrackPlayer::next(l_this.clone());
                    }
                    else {
                        drop(t);
                        for c in listeners {
                            c();
                        }
                    }
                }
            }
        }));

        t.timer = Some(tokio::spawn(async move {
            let mut interval = time::interval(Duration::from_millis(ELAPSED_POLL_RATE));
            loop {
                interval.tick().await;
                tx.send(Duration::from_millis(ELAPSED_POLL_RATE)).unwrap();
            }
        }));

        Ok(())
    }

    pub fn incr_elapsed(this: Arc<Mutex<Self>>, incr: Duration) {
        let mut t = this.lock().unwrap();
        t.elapsed.add_assign(incr);
    }

    // TODO: Handle time > duration
    pub fn scrub_to_time(this: Arc<Mutex<TrackPlayer>>, time: u64) -> Result<()> {
        let curr_track = TrackPlayer::current(this.clone());
        TrackPlayer::play_track(this, curr_track, Duration::from_secs(time))?;
        Ok(())
    }

    pub fn toggle_pause(this: Arc<Mutex<Self>>) {
        let t = this.lock().unwrap();
        if t.sink.is_paused() {
            drop(t);
            TrackPlayer::play(this);
        } else {
            drop(t);
            TrackPlayer::pause(this);
        }
    }

    pub fn pause(this: Arc<Mutex<Self>>) {
        let t = this.lock().unwrap();
        t.sink.pause();
        let listeners = t.play_status_change_listeners.clone();
        drop(t);
        for c in listeners {
            c();
        }
    }

    pub fn play(this: Arc<Mutex<Self>>) {
        let t = this.lock().unwrap();
        t.sink.play();
        let listeners = t.play_status_change_listeners.clone();
        drop(t);
        for c in listeners {
            c();
        }
    }
}

