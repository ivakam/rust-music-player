use std::collections::{HashMap, HashSet};
use std::ffi::OsStr;
use std::fs;
use std::fs::File;
use std::io::{Cursor, Read, Write};
use std::path::{Path, PathBuf};
use std::time::Duration;
use anyhow::{Context, Result};
use audiotags::Tag;
use http::Uri;
use image::EncodableLayout;
use log::info;
use regex::Regex;
use image::io::Reader as ImageReader;
use crate::{Album, AlbumGroupTree, AppConfig, DEFAULT_COVER, ID, Track};
use crate::backend::structs::AlbumGroup;
use crate::match_tags;

pub fn load_config() -> Result<AppConfig> {
    let config_file = File::open("config.toml");
    match config_file {
        Ok(mut f) => {
            let mut s = String::new();
            f.read_to_string(&mut s)
                .context("Could not read config file")?;
            let cfg = toml::from_str(&s).context("Failed to parse config file")?;
            Ok(cfg)
        }
        Err(_) => {
            let cfg = AppConfig::generate_default();
            let mut f =
                File::create("config.toml").context("Failed to create default config file")?;
            let cfg_toml = toml::to_vec(&cfg).context("Failed to serialize default config")?;
            f.write_all(cfg_toml.as_bytes())
                .context("Failed to write to config file")?;
            Ok(cfg)
        }
    }
}

pub fn scan_for_albums(cfg: &mut AppConfig) -> Result<(HashMap<ID,Album>, HashMap<ID,Track>, AlbumGroupTree)> {
    info!("Loading albums...");
    let master_albums;
    let mut master_tracks;

    let album_cache_file = File::open(".cache/album-cache");
    let track_cache_file = File::open(".cache/track-cache");
    match track_cache_file {
        Ok(f) => {
            info!("Found track cache...");
            master_tracks = rmp_serde::from_read(f).context("Failed to parse track cache")?;
        }
        Err(_) => {
            info!("No cache found, building new...");
            master_tracks = scan_for_tracks(&cfg.library_path);
        }
    }
    match album_cache_file {
        Ok(f) => {
            info!("Found album cache...");
            master_albums = rmp_serde::from_read(f).context("Failed to parse album cache")?;
        }
        Err(_) => {
            info!("No cache found, building new...");
            master_albums = build_album_database(&mut master_tracks);
            cache_albums(&master_albums)?;
            cache_tracks(&master_tracks)?
        }
    }
    let album_set: HashSet<ID> = master_albums.keys().cloned().collect();
    let tag_set: HashSet<&str> = HashSet::from(["artist", "genre", "year"]);
    info!("Generating group mappings...");
    let grouped_albums = get_tag_mappings(&master_albums, tag_set, album_set);

    Ok((master_albums, master_tracks, AlbumGroupTree::new(grouped_albums)))
}

pub fn load_asset(path: &str) -> Uri {
    match path.parse::<Uri>() {
        Ok(asset) => asset,
        Err(err) => {
            panic!("Failed to load asset from path {}: {}", path, err);
        }
    }
}

pub fn scan_for_tracks(dir: &String) -> HashMap<ID, Track> {
    if fs::create_dir(".cache").is_err() {
        todo!()
    }

    // TODO: Error handling
    let paths = find_music_files_rec(dir.clone()).expect("Failed to find music files");
    make_track_from_file_paths(paths).expect("Failed to make tracks from found files")
}

pub fn build_album_database(tracks: &mut HashMap<ID, Track>) -> HashMap<ID, Album> {
    make_albums_from_tracks(tracks)
}

// TODO: Fix scenario where two albums share a name
pub fn get_tag_mappings(
    master_albums: &HashMap<ID, Album>,
    tags: HashSet<&str>,
    albums: HashSet<ID>,
) -> AlbumGroup {
    if tags.is_empty() {
        return AlbumGroup::AlbumSet(albums);
    }
    let mut g: HashMap<String, AlbumGroup> = HashMap::new();
    for tag in &tags {
        let mut album_set: HashMap<String, HashSet<ID>> = HashMap::new();
        let tag = *tag;
        // Extract all unique values for the tag from the album set and insert into
        // a temporary AlbumGroup::Group. In other words, each album is mapped to
        // its value for the current tag like genre or artist.
        match_tags!(tag, albums, master_albums, album_set, [artist, genre, year]);

        // Clone the set of tags and remove the matched tag to prevent infinite recursion.
        let mut new_tags = tags.clone();
        new_tags.remove(tag);
        // Recursively call this function on each of the tag values using the mapped albums
        // in a_set as the set of albums for it to further group by.
        let mut map: HashMap<String, AlbumGroup> = HashMap::new();
        for (key, ids) in album_set {
            map.insert(key, get_tag_mappings(master_albums, new_tags.clone(), ids));
        }
        g.insert(tag.to_string(), AlbumGroup::Group(map));
    }
    AlbumGroup::Group(g)
}

fn cache_albums(albums: &HashMap<ID, Album>) -> Result<()> {
    let serialized_albums = rmp_serde::to_vec(albums)?;
    let mut f = File::create(".cache/album-cache").context("Failed to create album cache")?;
    f.write_all(serialized_albums.as_bytes())?;
    Ok(())
}

fn cache_tracks(tracks: &HashMap<ID, Track>) -> Result<()> {
    let serialized_tracks = rmp_serde::to_vec(tracks)?;
    let mut f = File::create(".cache/track-cache").context("Failed to create track cache")?;
    f.write_all(serialized_tracks.as_bytes())?;
    Ok(())
}

fn make_albums_from_tracks(tracks: &mut HashMap<ID, Track>) -> HashMap<ID, Album> {
    let mut res: HashMap<ID, Album> = HashMap::new();
    let mut found_albums: HashMap<String, HashSet<ID>> = HashMap::new();

    for (id, t) in tracks.clone() {
        if !found_albums.contains_key(&t.album) {
            found_albums.insert(t.album.clone(), HashSet::new());
        }
        found_albums.get_mut(&t.album).unwrap().insert(id);
    }

    for (title, ids) in found_albums {
        let album_id = ID::new();
        let mut ts = HashSet::<ID>::new();
        for id in &ids {
            ts.insert(*id);
            tracks.get_mut(id).unwrap().album_id = album_id;
        }
        let t_sample = tracks[ids.iter().next().unwrap()].clone();

        let p = t_sample.file_path;
        let mut cover = String::from(DEFAULT_COVER);

        if let Ok(pa) = try_cache_cover(p.clone(), album_id) {
            cover = pa;
        }

        let mut encoded_uri = String::from("");
        url_escape::encode_path_to_string(cover.as_str(), &mut encoded_uri);
        encoded_uri = "/".to_string() + &encoded_uri;

        let a = Album::new(
            album_id,
            title.clone(),
            t_sample.artist.clone(),
            t_sample.year.clone(),
            t_sample.genre.clone(),
            encoded_uri.clone(),
            ts,
        );
        res.insert(a.id, a);
    }

    res
}

fn find_music_files_rec(path: String) -> Result<Vec<String>, std::io::Error> {
    let re: Regex = Regex::new(r".+\.(mp3|m4a|flac)$").unwrap();
    let mut res: Vec<String> = Vec::new();

    let metadata = fs::metadata(&path)?;

    if re.is_match(path.as_str()) {
        res.push(path);
    } else if metadata.is_dir() {
        for entry in fs::read_dir(path)? {
            let entry = entry?;
            let path = String::from(entry.path().to_str().unwrap());
            let mut to_append = find_music_files_rec(path)?;
            res.append(to_append.as_mut());
        }
    }

    Ok(res)
}

fn make_track_from_file_paths(paths: Vec<String>) -> Result<HashMap<ID, Track>, audiotags::Error> {
    let mut res: HashMap<ID, Track> = HashMap::new();

    for p in paths {
        let id = ID::new();
        let mut title = String::from("No title");
        let mut artist = String::from("No artist");
        let mut album = String::from("No album");
        let mut genre = String::from("No genre");
        let mut year = String::from("NaN");
        let mut duration = Duration::from_secs(0);
        let mut track_number = 0;
        let file_path = p.as_str();

        let tag = Tag::new().read_from_path(p.clone());
        match tag {
            Ok(tag) => {
                if let Some(t) = tag.title() {
                    title = t.to_string();
                }
                if let Some(t) = tag.artist() {
                    artist = t.to_string();
                }
                if let Some(t) = tag.album_title() {
                    album = t.to_string();
                }
                if let Some(t) = tag.genre() {
                    genre = t.to_string();
                }
                if let Some(t) = tag.year() {
                    year = t.to_string();
                }
                match tag.duration() {
                    Some(t) => duration = Duration::from_secs(t as u64),
                    None => {
                        if let Some(d) = try_get_duration(&p) {
                            duration = d;
                        }
                    }
                }
                if let Some(t) = tag.track_number() {
                    track_number = t
                }
            }
            Err(e) => {
                if let audiotags::Error::Id3TagError(_) = e {
                    let fallback_tag = id3::v1::Tag::read_from_path(p.clone());
                    if let Ok(t) = fallback_tag {
                        title = t.title;
                        artist = t.artist;
                        year = t.year;
                        album = t.album;
                        if let Some(t) = t.genre_str {
                            genre = t
                        }
                        match try_get_duration(&p) {
                            Some(d) => duration = d,
                            None => {
                                if let Some(t) = t.end_time {
                                    duration = Duration::from_secs(t.parse::<u64>().unwrap())
                                }
                            }
                        }
                        if let Some(t) = t.track {
                            track_number = t as u16
                        }
                    }
                }
            }
        }

        let t = Track::new(
            id,
            ID::from_u32(0),
            title,
            artist,
            track_number,
            album,
            year,
            genre,
            duration,
            file_path.to_string(),
        );
        res.insert(t.id, t);
    }

    Ok(res)
}

fn try_get_duration(path: &str) -> Option<Duration> {
    let p = Path::new(path);
    let ext = p.extension().and_then(OsStr::to_str).unwrap();
    match ext {
        "mp3" => {
            match mp3_duration::from_path(p){
                Ok(dur) => Some(dur),
                Err(e) => None,
            }
        },
        "flac" => None,
        _ => None,
    }
}

fn try_cache_cover(path: String, id: ID) -> Result<String, image::ImageError> {
    let re = Regex::new(r"(?i)(?P<file>cover|folder)\.(?P<mime>png|jpg|jpeg|gif|bmp|tiff|tif)$")
        .unwrap();
    let mut cache_path = format!(".cache/{}-cover.jpg", id.inner());
    let mut res = String::from(DEFAULT_COVER);

    let buff = PathBuf::from(path.as_str());
    let parent = buff.parent().unwrap();
    for sibling in fs::read_dir(parent).unwrap() {
        let sibling = sibling.unwrap();
        let path = String::from(sibling.path().to_str().unwrap());
        if re.is_match(path.as_str()) {
            let caps = re.captures(path.as_str()).unwrap();
            if let Some(r) = caps.name("mime") {
                cache_path = format!(".cache/{}-cover.{}", id.inner(), r.as_str());
            }
            let _ = fs::copy(&path, &cache_path);
            res = cache_path;
            break;
        }
    }

    if res == DEFAULT_COVER {
        let tag = Tag::new().read_from_path(path);
        if let Ok(tag) = tag {
            if let Some(t) = tag.album_cover() {
                let img = ImageReader::new(Cursor::new(t.data))
                    .with_guessed_format()?
                    .decode()
                    .unwrap();

                match t.mime_type {
                    audiotags::MimeType::Png => {
                        cache_path = format!(".cache/{}-cover.png", id.inner());
                        let _ = img.save(&cache_path);
                        res = cache_path;
                    }
                    audiotags::MimeType::Jpeg => {
                        cache_path = format!(".cache/{}-cover.jpg", id.inner());
                        let _ = img.save(&cache_path);
                        res = cache_path;
                    }
                    audiotags::MimeType::Bmp => {
                        cache_path = format!(".cache/{}-cover.bmp", id.inner());
                        let _ = img.save(&cache_path);
                        res = cache_path;
                    }
                    audiotags::MimeType::Gif => {
                        cache_path = format!(".cache/{}-cover.gif", id.inner());
                        let _ = img.save(&cache_path);
                        res = cache_path;
                    }
                    audiotags::MimeType::Tiff => {
                        cache_path = format!(".cache/{}-cover.tif", id.inner());
                        let _ = img.save(&cache_path);
                        res = cache_path;
                    }
                }
            }
        }
    }
    Ok(res)
}

#[macro_export]
macro_rules! match_tags {
    ( $tag:ident, $albums:ident, $master_albums:ident, $album_set:ident, [$( $x:ident ),*] ) => {
        {
            match $tag {
                    $(
                    stringify!($x) => {
                        for id in &$albums {
                            let k = match $master_albums.get(id) {
                                Some(album) => album.$x.clone(),
                                None => {
                                    panic!("Could not find album with id {}", id)
                                },
                            };
                            if !$album_set.contains_key(&k) {
                                $album_set.insert(k.clone(), HashSet::new());
                            }
                            let set = &mut $album_set.get_mut(&k).unwrap();
                            set.insert(*id);
                        }
                    }
                    )*
                _ => {
                    // TODO: Raise error
                }
            }
        }
    };
}
