use std::collections::{HashMap, HashSet};
use std::fmt::Display;
use std::sync::Arc;
use std::time::Duration;

use crate::backend::structs::AlbumGroup::{AlbumSet, Group};
use crate::backend::structs::SortOpt::{Asc, Desc};
use serde::{Deserialize, Serialize};
use zvariant::{DeserializeDict, OwnedValue, SerializeDict, Type, Value};

#[derive(Serialize, Deserialize, Clone)]
pub struct AppConfig {
    pub library_path: String,
    pub theme_path: String,
    pub bg_opacity: f32,
}

impl AppConfig {
    pub fn new(library_path: String, theme_path: String, bg_opacity: f32) -> Self {
        AppConfig {
            library_path,
            theme_path,
            bg_opacity,
        }
    }

    pub fn generate_default() -> Self {
        AppConfig {
            library_path: "/home/music".to_string(),
            theme_path: "src/style.css".to_string(),
            bg_opacity: 0.5,
        }
    }
}

#[derive(Copy, Clone, Hash, Eq, PartialEq, Debug, Serialize, Deserialize)]
pub struct ID(u32);

impl ID {
    pub fn new() -> Self {
        ID(rand::random::<u32>())
    }
    pub fn from_u32(id: u32) -> Self {
        ID(id)
    }

    pub fn inner(&self) -> u32 {
        self.0
    }
}

impl Display for ID {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(DeserializeDict, SerializeDict, Type, Value, OwnedValue, Clone)]
#[zvariant(signature = "dict")]
pub struct Dictionary {
    #[zvariant(rename = "xesam:title")]
    pub(crate) f1: String,
    #[zvariant(rename = "xesam:artist")]
    pub(crate) f2: Vec<String>,
    #[zvariant(rename = "xesam:album")]
    pub(crate) f3: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Track {
    pub id: ID,
    pub album_id: ID,
    pub title: String,
    pub artist: String,
    pub track_number: u16,
    pub album: String,
    pub year: String,
    pub genre: String,
    pub duration: Duration,
    pub file_path: String,
}

impl Track {
    pub fn new(
        id: ID,
        album_id: ID,
        title: String,
        artist: String,
        track_number: u16,
        album: String,
        year: String,
        genre: String,
        duration: Duration,
        file_path: String,
    ) -> Self {
        Track {
            id,
            album_id,
            title,
            artist,
            track_number,
            album,
            year,
            genre,
            duration,
            file_path,
        }
    }
    /*
    LEAVE BE, This is the better way to do this, but is currently impossible due to bugs in zbus.
     */
    //pub fn to_mpris(&self) -> Dictionary {
    //    Dictionary {
    //        f1: self.title.clone(),
    //        f2: vec!(self.artist.clone()),
    //        f3: self.album.clone(),
    //    }
    //}
    pub fn to_mpris(&self) -> HashMap<String, Value<'_>> {
        let cwd = std::env::current_dir().expect("Failed to get working directory");
        let cover_path = format!(".cache/{}-cover.jpg", self.album_id.inner().to_string());
        let p = cwd.join(cover_path);
        let p = format!("file://{}", p.to_str().unwrap());
        let mut res = HashMap::new();
        res.insert("xesam:title".to_string(), Value::from(self.title.clone()));
        res.insert(
            "xesam:artist".to_string(),
            Value::from(vec![self.artist.clone()]),
        );
        res.insert("xesam:album".to_string(), Value::from(self.album.clone()));
        res.insert("mpris:artUrl".to_string(), Value::from(p));
        res
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Album {
    pub id: ID,
    pub title: String,
    pub artist: String,
    pub year: String,
    pub genre: String,
    pub cover: String,
    pub tracks: HashSet<ID>,
}

impl Album {
    pub fn new(
        id: ID,
        title: String,
        artist: String,
        year: String,
        genre: String,
        cover: String,
        tracks: HashSet<ID>,
    ) -> Self {
        Album {
            id,
            title,
            artist,
            year,
            genre,
            cover,
            tracks,
        }
    }

    pub fn get_sorted_tracks(&self, master_tracks: Arc<HashMap<ID, Track>>) -> Vec<ID> {
        let mut ts: Vec<ID> = Vec::new();
        for t in &self.tracks {
            ts.push(*t)
        }

        ts.sort_by(|a, b| {
            let a = &master_tracks[a];
            let b = &master_tracks[b];
            a.track_number.cmp(&b.track_number)
        });
        ts.to_vec()
    }
}

pub enum SortOpt {
    // The strings in the variants are the names of the groups
    Asc(String),
    Desc(String),
}

/*
 * Recursive type for use in track group mappings.
 */
#[derive(PartialEq, Clone, Debug)]
pub enum AlbumGroup {
    AlbumSet(HashSet<ID>),
    // The string is the name or value of a tag such as "artist" or "Pop"
    Group(HashMap<String, AlbumGroup>),
}

impl AlbumGroup {
    // TODO: Implement multi-variable sorting
    pub fn to_sorted_vec(
        &self,
        master_albums: Arc<HashMap<ID, Album>>,
        sort_order: Vec<SortOpt>,
    ) -> Vec<ID> {
        let mut res = Vec::from_iter(self.flatten());
        for opt in sort_order {
            res.sort_by(|a, b| {
                let alb_a = &master_albums[a];
                let alb_b = &master_albums[b];
                match &opt {
                    Asc(o) => match o.as_str() {
                        "title" => alb_a.title.cmp(&alb_b.title),
                        "artist" => alb_a.artist.cmp(&alb_b.artist),
                        "year" => alb_a.year.cmp(&alb_b.year),
                        "genre" => alb_a.genre.cmp(&alb_b.genre),
                        _ => alb_a.title.cmp(&alb_b.title),
                    },
                    Desc(o) => match o.as_str() {
                        "title" => alb_b.title.cmp(&alb_a.title),
                        "artist" => alb_b.artist.cmp(&alb_a.artist),
                        "year" => alb_b.year.cmp(&alb_a.year),
                        "genre" => alb_b.genre.cmp(&alb_a.genre),
                        _ => alb_b.title.cmp(&alb_a.title),
                    },
                }
            });
        }
        res
    }

    pub fn follow_path(&self, path: &Vec<String>) -> AlbumGroup {
        let mut res: HashMap<String, AlbumGroup> = HashMap::new();
        if let Group(g) = self {
            res = g.clone();
            for step in path {
                match &res[step] {
                    Group(gr) => res = gr.clone(),
                    AlbumSet(_) => {
                        return res[step].clone();
                    }
                }
            }
        }
        Group(res)
    }

    // Returns a flattened version of the AlbumGroup as an AlbumsSet
    fn flatten(&self) -> HashSet<ID> {
        let mut res: HashSet<ID> = HashSet::new();
        match self {
            Group(g) => g.values().for_each(|v| res.extend(v.flatten())),
            AlbumSet(s) => res.extend(s),
        }
        res
    }
}
