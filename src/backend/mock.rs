use std::collections::HashMap;
use std::time::Duration;
use rand::prelude::SliceRandom;
use rand::Rng;
use crate::{ID, Track};

static TRACK_NAMES: [&str; 16] = [
    "Up Around The Bend",
    "Trädgården en fredag",
    "Out on the Weekend",
    "Upstate Blues",
    "Mitt Sår",
    "Chinchilla",
    "Sexy Dance",
    "The Good Thing",
    "Crystal Mountain",
    "Ramble Tamble",
    "Baboon",
    "Heart of Gold",
    "Låtsas som det regnar",
    "Love E.S.P.",
    "Symbolic",
    "Heard it through the Grape Vine",
];

static ALBUM_NAMES: [&str; 10] = [
    "Cosmo's Factory",
    "Handen i fickan fast jag bryr mig",
    "Harvest",
    "Niagara Moon",
    "Goda' Goda'",
    "Animals",
    "Symbolic",
    "An Insatiable High",
    "Intersections",
    "More Songs About Buildings And Food",
];

static ARTIST_NAMES: [&str; 12] = [
    "Creedence Clearwater Revival",
    "Veronica Maggio",
    "Neil Young",
    "Eiichi Ohtaki",
    "Jojje Wadenius",
    "TTNG",
    "Into It. Over It.",
    "Masayoshi Takanaka",
    "Death",
    "ABBA",
    "Talking Heads",
    "Charlie XCX",
];

static GENRES: [&str; 11] = [
    "Classic Rock",
    "Rock",
    "Alternative Rock",
    "Emo",
    "Pop",
    "Hyper Pop",
    "Funk",
    "Metal",
    "Ambient",
    "EDM",
    "Blues",
];

pub fn generate_mock_tracks(album_amount: u16) -> HashMap<ID, Track> {
    let mut res: HashMap<ID, Track> = HashMap::new();
    for i in 0..album_amount {
        let artist = ARTIST_NAMES
            .choose(&mut rand::thread_rng())
            .unwrap()
            .to_string();
        let album_title = ALBUM_NAMES
            .choose(&mut rand::thread_rng())
            .unwrap()
            .to_string()
            + &i.to_string();
        let genre = GENRES.choose(&mut rand::thread_rng()).unwrap().to_string();
        let year = rand::thread_rng().gen_range(1970..1975).to_string();
        let track_count = rand::thread_rng().gen_range(5..30);
        for j in 1..track_count {
            let t = Track::new(
                ID::new(),
                ID::from_u32(0),
                TRACK_NAMES
                    .choose(&mut rand::thread_rng())
                    .unwrap()
                    .to_string(),
                artist.clone(),
                j,
                album_title.clone(),
                year.clone(),
                genre.clone(),
                Duration::from_secs(rand::thread_rng().gen_range(250..800)),
                "".to_string(),
            );
            res.insert(t.id, t);
        }
    }
    res
}

