use std::sync::Arc;
use dioxus::prelude::{Props, UseRef};
use crate::backend::structs::{AlbumGroup, ID};
use crate::components::newtypes::{Grouping, MainPlayer, MainSink};

/****************************
 * DIOXUS PROPS AND STRUCTS *
 ****************************/

pub struct AppProps {
    pub sink: MainSink,
}

#[derive(PartialEq, Eq, Props)]
pub struct BodyProps {}

#[derive(PartialEq, Props)]
pub struct PlayerProps<'a> {
    pub player: &'a UseRef<MainPlayer>,
}

#[derive(PartialEq, Props)]
pub struct BodyBgProps<'a> {
    pub player: &'a UseRef<MainPlayer>,
}

#[derive(PartialEq, Props)]
pub struct DrawerProps<'a> {
    pub player: &'a UseRef<MainPlayer>,
    pub selected_group_tree_path: &'a UseRef<Grouping>,
}

#[derive(PartialEq, Props)]
pub struct DrawerItemProps<'a> {
    pub layer: usize,
    pub group: Arc<AlbumGroup>,
    // The full grouping tree path that the drawer should
    // maximally group to
    pub total_group_tree_path: &'a UseRef<Grouping>,
    // The group tree path to the currently selected drawer item
    pub selected_group_tree_path: &'a UseRef<Grouping>,
    // The group tree path to the current drawer item
    pub current_group_tree_path: Grouping,
}

#[derive(PartialEq, Props)]
pub struct DrawerSettingsProps<'a> {
    pub total_grouping: &'a UseRef<Grouping>,
}

#[derive(PartialEq, Props)]
pub struct DrawerSettingsItemProps<'a> {
    pub total_grouping: &'a UseRef<Grouping>,
    pub index: usize,
}

#[derive(PartialEq, Props)]
pub struct BrowserProps<'a> {
    pub player: &'a UseRef<MainPlayer>,
    pub selected_group: &'a UseRef<Grouping>,
}

#[derive(PartialEq, Props)]
pub struct BrowserAlbumProps<'a> {
    pub player: &'a UseRef<MainPlayer>,
    pub album_id: ID,
    pub selected_track: &'a UseRef<ID>,
}

#[derive(PartialEq, Props)]
pub struct TrackInfoProps<'a> {
    pub player: &'a UseRef<MainPlayer>,
    pub id: ID,
    pub selected_track: &'a UseRef<ID>,
}

#[derive(PartialEq, Props)]
pub struct ControlsProps<'a> {
    pub player: &'a UseRef<MainPlayer>,
}
