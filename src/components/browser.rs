use crate::backend::track_player::TrackPlayer;
use dioxus::prelude::*;
use http::Uri;
use crate::backend::structs::ID;
use crate::components::props::{BrowserAlbumProps, BrowserProps, TrackInfoProps};

#[allow(non_snake_case)]
pub fn Browser<'a>(cx: Scope<'a, BrowserProps>) -> Element<'a> {
    let player = cx.props.player;

    let albums_with_sorted_tracks = TrackPlayer::setup_tracks(player.read().inner_c(), cx.props.selected_group.read().inner());
    let selected_track = use_ref(&cx, || ID::from_u32(0));

    cx.render(rsx! {
        div { class: "browser",
            // Album list browser, should be refactored later
            ul { class: "browser-album-list",
                albums_with_sorted_tracks.iter().map(|(a_id, ts)| {
                    rsx!{
                        BrowserAlbum {
                            player: cx.props.player,
                            key: "{a_id}",
                            album_id: a_id.clone(),
                            selected_track: selected_track,
                        }
                    }
                })
            }
        }
    })
}

#[allow(non_snake_case)]
fn BrowserAlbum<'a>(cx: Scope<'a, BrowserAlbumProps>) -> Element<'a> {
    let player = cx.props.player.read();
    let album = TrackPlayer::get_album_from_id(player.inner_c(), cx.props.album_id);

    let tracks: Vec<ID> = TrackPlayer::get_tracks_for_album(player.inner_c(), cx.props.album_id);

    cx.render(rsx! {
        li { class: "browser-album",
            div { class: "album-header",
                img { class: "album-header-cover-img",
                    src: "{album.cover}",
                }
                div { class: "album-header-text-container",
                    p { class: "album-title-text",
                        "{album.title}"
                    }
                    p { class: "album-artist-text",
                        "{album.artist}"
                    }
                    p { class: "album-year-text",
                        "{album.year}"
                    }
                }
            }
            div { class: "track-list-header",
                p { "Nr." }
                p { "Title"}
                p { "Artist" }
                p { "Length" }
            }
            ul { class: "browser-track-list",
                tracks.iter().map(move |t| {
                    rsx!{
                        TrackInfo {
                            player: cx.props.player,
                            key: "{t}",
                            id: t.clone(),
                            selected_track: cx.props.selected_track,
                        }
                    }
                })
            }
        }
    })
}

#[allow(non_snake_case)]
fn TrackInfo<'a>(cx: Scope<'a, TrackInfoProps>) -> Element<'a> {
    let player = cx.props.player.read();

    TrackPlayer::add_track_change_listener(player.inner_c(), cx.schedule_update());

    let selected_track = cx.props.selected_track;
    let track = TrackPlayer::get_track_from_id(player.inner_c(), cx.props.id);

    let mut cls_str = "track-info".to_string();
    let is_selected = *selected_track.read() == cx.props.id;
    let is_playing = TrackPlayer::current(player.inner_c()) == cx.props.id;
    if is_selected {
        cls_str.push_str(" selected");
    }

    let play = match "/assets/play-button.svg".parse::<Uri>() {
        Ok(parsed) => parsed,
        Err(err) => {
            panic!("Failed to load asset: {}", err);
        }
    };

    let hidden = if is_playing { "" } else { "hidden" };

    let duration = track.duration;
    let minutes = duration.as_secs() / 60;
    let seconds = duration.as_secs() % 60;
    let duration = format!("{}:{:0>2}", minutes, seconds);

    cx.render(rsx! {
        li { class: "{cls_str}",
            onclick: move |e| {
                e.cancel_bubble();
                if !is_selected{
                    selected_track.set(cx.props.id)
                }
                else {
                    let p = cx.props.player.write_silent();
                    TrackPlayer::find_and_play(p.inner_c(), cx.props.id);
                }
            },
            if is_playing {rsx!{
                img { class: "track-playing-icon {hidden}",
                    src: "{play}",
                }
            }}
            else {rsx!{div{}}}
            p { "{track.track_number}" }
            p { "{track.title}" }
            p { "{track.artist}" }
            p {"{duration}" }
        }
    })
}
