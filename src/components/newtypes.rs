use std::sync::{Arc, Mutex};
use rodio::Sink;
use crate::backend::track_player::TrackPlayer;
use crate::backend::structs::AlbumGroup;

/*
 * Wrapper newtype for Dioxus context
 */
#[derive(Clone)]
pub struct MainSink(Arc<Sink>);

impl MainSink {
    pub fn new(sink: Sink) -> Self {
        MainSink(Arc::new(sink))
    }

    pub fn inner(&self) -> &Arc<Sink> {
        &self.0
    }
}

/*
 * Wrapper newtype for Dioxus context
 */
#[derive(Clone)]
pub struct AlbumGroupTree(Arc<AlbumGroup>);

impl AlbumGroupTree {
    pub fn new(grp: AlbumGroup) -> Self {
        AlbumGroupTree(Arc::new(grp))
    }

    pub fn inner(&self) -> &Arc<AlbumGroup> {
        &self.0
    }

    pub fn inner_c(&self) -> Arc<AlbumGroup> {
        self.0.clone()
    }

    pub fn follow_path(&self, path: &Vec<String>) -> AlbumGroup {
        self.0.follow_path(path)
    }
}

/*
 * Wrapper newtype for Dioxus context
 */
#[derive(Clone)]
pub struct MainPlayer(Arc<Mutex<TrackPlayer>>);

impl MainPlayer {
    pub fn new(player: TrackPlayer) -> Self {
        MainPlayer(Arc::new(Mutex::new(player)))
    }

    pub fn inner(&self) -> &Arc<Mutex<TrackPlayer>> {
        &self.0
    }

    pub fn inner_c(&self) -> Arc<Mutex<TrackPlayer>> {
        self.0.clone()
    }
}

#[derive(PartialEq, Eq, Clone)]
pub struct Grouping(Vec<String>);

impl Grouping {
    pub fn new(vec: Vec<String>) -> Self {
        Grouping(vec)
    }

    pub fn inner(&self) -> &Vec<String> {
        &self.0
    }

    pub fn inner_mut(&mut self) -> &mut Vec<String> {
        self.0.as_mut()
    }
}
