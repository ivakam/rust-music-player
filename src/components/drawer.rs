use dioxus::prelude::*;
use std::collections::HashMap;
use std::ops::Deref;
use std::sync::Arc;
use crate::backend::structs::AlbumGroup::Group;
use crate::backend::io_utils;
use crate::backend::track_player::TrackPlayer;
use crate::util;
use crate::components::newtypes::{Grouping};
use crate::components::props::{DrawerItemProps, DrawerProps, DrawerSettingsItemProps, DrawerSettingsProps};

#[allow(non_snake_case)]
pub fn Drawer<'a>(cx: Scope<'a, DrawerProps>) -> Element<'a> {
    let total_grouping = use_ref(&cx, || {
        Grouping::new(vec![
            "genre".to_string(),
            "year".to_string(),
            "artist".to_string(),
        ])
    });
    let show_drawer = use_state(&cx, || true);
    let show_settings = use_state(&cx, || false);
    let grouped_albums = TrackPlayer::group_tree(cx.props.player.read().inner_c());

    let init_group = grouped_albums.inner_c();
    let settings = io_utils::load_asset("/assets/settings.svg");

    if !show_drawer {
        return cx.render(rsx! {
            div { class: "drawer-toggle-btn mirrored",
                onclick: move |_e| { show_drawer.set(!**show_drawer) },
                div { class: "triangle-left" }
            }
        });
    }

    let content = if **show_settings {
        rsx! {
            DrawerSettings {
                total_grouping: total_grouping,
            }
        }
    } else {
        rsx! {
            DrawerItem {
                layer: usize::MIN,
                group: init_group.clone(),
                total_group_tree_path: total_grouping,
                selected_group_tree_path: cx.props.selected_group_tree_path,
                current_group_tree_path: Grouping::new(Vec::<String>::new())
            }
        }
    };
    cx.render(rsx! {
        ul { class: "drawer",
            div { class: "drawer-toggle-btn",
                onclick: move |_e| { show_drawer.set(!**show_drawer) },
                div { class: "triangle-left" }
            }
            div { class: "drawer-grouping-container",
                div { class: "drawer-grouping-text",
                    total_grouping.read().inner().iter().map(|group| {
                        let group = util::capitalize_first(group);
                        rsx! {
                            p { "{group}" }
                        }
                    })
                }
                img { class: "grouping-settings",
                    src: "{settings}",
                    onclick: move |_e| { show_settings.set(!**show_settings) }
                }
            }
            div { class: "drawer-content",
                content
            }
        }
    })
}

#[allow(non_snake_case)]
fn DrawerItem<'a>(cx: Scope<'a, DrawerItemProps>) -> Element<'a> {
    let layer = cx.props.layer;
    let group = &cx.props.group;
    let grouping = cx.props.total_group_tree_path;
    let selected_grp_tree_path = cx.props.selected_group_tree_path;

    let open = use_ref(&cx, HashMap::<String, bool>::new);
    let hovered = use_state(&cx, || false);

    if layer < cx.props.total_group_tree_path.read().inner().len() {
        return match group.deref() {
            Group(map) => {
                let current_tag_label = &cx.props.total_group_tree_path.read();
                let current_tag_label = &current_tag_label.inner()[layer];
                let new_group = &map[current_tag_label];
                let n_layer = layer + 1;
                let mut res = rsx! {div{}};
                if let Group(n_group) = new_group {
                    res = rsx! {
                        n_group.iter().map(|(tag_val, _)| {
                            let t_val = tag_val.clone();
                            let mut curr_path_vec = cx.props.current_group_tree_path.inner().to_vec();
                            curr_path_vec.push(current_tag_label.clone());
                            curr_path_vec.push(tag_val.clone());
                            let curr_grp_tree_path = Grouping::new(curr_path_vec);
                            let is_currently_selected = curr_grp_tree_path == *selected_grp_tree_path.read();
                            let new_path = curr_grp_tree_path.clone();
                            if !open.read().contains_key(tag_val) {
                                open.with_mut(|map| map.insert(tag_val.clone(), false));
                            }
                            let mut d_tag_cls = String::from("drawer-tag");
                            if **hovered {
                                d_tag_cls.push_str(" hovered");
                            }
                            if is_currently_selected {
                                d_tag_cls.push_str(" selected")
                            }
                            rsx! {
                                div { class: "{d_tag_cls}",
                                    id: "drawer-{layer}-{t_val}",
                                    onclick: move |e| {
                                        if is_currently_selected{
                                            // FIXME: Unsafe indexing
                                            if open.read()[tag_val] {
                                                e.cancel_bubble();
                                                open.with_mut(|map| map.insert(tag_val.clone(), false));
                                            }
                                            else {
                                                e.cancel_bubble();
                                                open.with_mut(|map| map.insert(tag_val.clone(), true));
                                            }
                                        }
                                        else {
                                            selected_grp_tree_path.set(new_path.clone());
                                        }
                                    },
                                    div { class: "drawer-tag-text-container",
                                        p { "{t_val}" }
                                    }
                                    // FIXME: Unsafe indexing
                                    if open.read()[&t_val] {
                                        let n_gr = n_group[tag_val].clone();
                                        rsx!{
                                            div { class: "drawer-children",
                                                DrawerItem {
                                                    layer: n_layer,
                                                    group: Arc::new(n_gr),
                                                    total_group_tree_path: grouping,
                                                    selected_group_tree_path: selected_grp_tree_path,
                                                    current_group_tree_path: curr_grp_tree_path,
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        rsx!{div{class: "hidden"}}
                                    }
                                }
                            }
                        })
                    };
                }
                cx.render(res)
            }
            _ => cx.render(rsx! {div{}}),
        };
    } else {
        cx.render(rsx! {div{}})
    }
}

#[allow(non_snake_case)]
fn DrawerSettings<'a>(cx: Scope<'a, DrawerSettingsProps>) -> Element<'a> {
    let total_grouping = cx.props.total_grouping;
    let add = io_utils::load_asset("/assets/add.svg");
    cx.render(rsx! {
        div { class: "drawer-settings-container",
            total_grouping.read().inner().iter().enumerate().map(|(i, _group)| {
                rsx! {
                    DrawerSettingsItem {
                        total_grouping: total_grouping,
                        index: i as usize,
                    }
                }
            })
            img { class: "add-grp-btn",
                src: "{add}",
                onclick: move |_e| {
                    total_grouping.with_mut(|gs| gs.inner_mut().push("artist".to_string()))
                }
            }
        }
    })
}

#[allow(non_snake_case)]
fn DrawerSettingsItem<'a>(cx: Scope<'a, DrawerSettingsItemProps>) -> Element<'a> {
    let total_grouping = cx.props.total_grouping;
    let idx = cx.props.index;
    let grp = total_grouping.read();
    let grp = match grp.inner().get(idx) {
        Some(g) => g,
        None => {
            panic!("Failed to get group for index {}", idx);
        }
    };

    // TODO: Eww
    let available_groups = ["artist", "year", "genre"];
    let remove = io_utils::load_asset("/assets/remove.svg");

    cx.render(rsx! {
        div { class: "drawer-settings-item",
            p { "{idx}." }
            // TODO: These should not be hardcoded
            select { class: "drawer-settings-input",
                onchange: move |e| {
                    total_grouping.with_mut(|gs| {
                        let gs: &mut Vec<String> = gs.inner_mut();
                        gs[idx] = e.value.clone()
                    })
                },
                available_groups.iter().map(|g| {
                    let g_cap = util::capitalize_first(g);
                    if g == grp {
                        rsx!{
                            option { value: "{g}", selected: "true", "{g_cap}" }
                        }
                    }
                    else {
                        rsx!{
                            option { value: "{g}", "{g_cap}" }
                        }
                    }
                })
            }
            img { class: "remove-grp-btn",
                src: "{remove}",
                onclick: move |_e| {
                    total_grouping.with_mut(|gs| {
                        let gs: &mut Vec<String> = gs.inner_mut();
                        gs.remove(idx);
                    })
                }
            }
        }
    })
}
