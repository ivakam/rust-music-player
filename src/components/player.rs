use crate::backend::track_player::TrackPlayer;
use crate::components::{ controls, drawer, browser };
use dioxus::prelude::*;
use crate::AppConfig;
use crate::components::newtypes::{ Grouping, MainPlayer, MainSink };
use crate::components::props::{BodyBgProps, BodyProps, PlayerProps};

#[allow(non_snake_case)]
pub fn Body(cx: Scope<BodyProps>) -> Element {
    // This state is prone to being lifted out to main.rs if needed
    let m_sink = cx.use_hook(|| cx.consume_context::<MainSink>().unwrap());
    let cfg = cx.use_hook(|| cx.consume_context::<AppConfig>().unwrap());

    let player = use_ref(&cx, || {
        let t = TrackPlayer::new(cfg, m_sink.inner().clone());
        let t = MainPlayer::new(t);
        TrackPlayer::init_dbus_connection(t.inner_c());
        t
    });

    cx.render(rsx! {
        div { class: "body-container",
            BodyBg { player: player }
            Player { player: player }
            controls::Controls { player: player }
        }
    })
}

#[allow(non_snake_case)]
fn Player<'a>(cx: Scope<'a, PlayerProps<'a>>) -> Element<'a> {
    let current_selection = use_ref(&cx, || Grouping::new(Vec::<String>::new()));

    cx.render(rsx! {
        section { class: "music-player",
            drawer::Drawer {
                player: cx.props.player,
                selected_group_tree_path: current_selection,
            }
            browser::Browser {
                player: cx.props.player,
                selected_group: current_selection,
            }
        }
    })
}

#[allow(non_snake_case)]
fn BodyBg<'a>(cx: Scope<'a, BodyBgProps<'a>>) -> Element<'a> {
    let player = cx.props.player.read();
    TrackPlayer::add_track_change_listener(player.inner_c(), cx.schedule_update());
    let cover = TrackPlayer::get_current_cover(player.inner_c());
    cx.render(rsx!{
        div { class: "body-bg",
            background_image: "url(\"{cover}\")",
        }
    })
}
