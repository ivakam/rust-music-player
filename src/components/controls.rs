use crate::backend::track_player::TrackPlayer;
use crate::backend::io_utils;
use crate::util;
use dioxus::prelude::*;
use log::error;
use crate::components::props::ControlsProps;

#[allow(non_snake_case)]
pub fn Controls<'a>(cx: Scope<'a, ControlsProps>) -> Element<'a> {
    let player = cx.props.player.read();
    TrackPlayer::add_elapsed_change_listener(player.inner_c(), cx.schedule_update());
    TrackPlayer::add_track_change_listener(player.inner_c(), cx.schedule_update());
    TrackPlayer::add_volume_change_listener(player.inner_c(), cx.schedule_update());
    TrackPlayer::add_play_status_change_listener(player.inner_c(), cx.schedule_update());

    let playing_track = TrackPlayer::current(player.inner_c());
    let playing_track = TrackPlayer::get_track_from_id(player.inner_c(), playing_track);
    let is_playing =
        TrackPlayer::is_active(player.inner_c()) && !TrackPlayer::is_paused(player.inner_c());

    let title = playing_track.title.clone();
    let artist = playing_track.artist.clone();
    let album = playing_track.album.clone();
    let cover = TrackPlayer::get_album_from_id(player.inner_c(), playing_track.album_id).cover;
    let cover = io_utils::load_asset(cover.as_str());

    let prev = io_utils::load_asset("/assets/play-track-prev.svg");
    let next = io_utils::load_asset("/assets/play-track-next.svg");
    let main_btn = if is_playing {
        "/assets/pause-button.svg"
    } else {
        "/assets/play-button.svg"
    };
    let main_btn = io_utils::load_asset(main_btn);
    let speaker = io_utils::load_asset("/assets/speaker.svg");

    let elapsed = TrackPlayer::elapsed(player.inner_c());
    let duration = TrackPlayer::duration(player.inner_c());
    let el_secs = elapsed.as_secs();
    let du_secs = duration.as_secs();
    let el_fmt = util::format_duration(elapsed);
    let du_fmt = util::format_duration(duration - elapsed);
    let scrubber_held = use_state(&cx, || false);
    let held_value = use_state(&cx, || 0);

    let volume = use_state(&cx, || 100.0);

    let scrubber_value = if *scrubber_held.current() {
        *held_value.current()
    } else {
        el_secs
    };

    cx.render(rsx! {
        section { class: "controls",
            div { class: "controls-container",
                div { class: "controls-info",
                    img { class: "controls-thumbnail",
                        src: "{cover}",
                    }
                    div { class: "controls-text",
                        p { class: "controls-title",
                            "{title}"
                        }
                        p { class: "controls-artist",
                            "{artist}"
                        }
                        p { class: "controls-album",
                            "{album}"
                        }
                    }
                }
                div { class: "controls-buttons",
                    img { class: "prev-btn",
                        src: "{prev}",
                        onclick: move |_| {
                            let p = cx.props.player.write_silent();
                            TrackPlayer::prev(p.inner_c());
                        },
                    }
                    img { class: "main-btn",
                        src: "{main_btn}",
                        onclick: move |_| {
                            let p = cx.props.player.write_silent();
                            TrackPlayer::toggle_pause(p.inner_c());
                        },
                    }
                    img { class: "next-btn",
                        src: "{next}",
                        onclick: move |_| {
                            let p = cx.props.player.write_silent();
                            TrackPlayer::next(p.inner_c());
                        },
                    }
                }
                div { class: "controls-volume",
                    input { class: "slider",
                        id: "volume-slider",
                        r#type: "range",
                        min: "0",
                        max: "100",
                        value: "{volume}",
                        oninput: move |e| {
                            let val = match e.value.parse::<f32>() {
                                Ok(val) => val,
                                Err(err) => {
                                    error!("Failed to parse volume slider value: {}", err);
                                    0f32
                                },
                            };
                            volume.with_mut(|v| *v = val);
                            let val = val / 100.0;
                            let p = cx.props.player.write_silent();
                            TrackPlayer::set_volume(p.inner_c(), val);
                        },
                        onwheel: move |e| {
                            if e.delta().strip_units().y < 0.0 {
                                volume.with_mut(|v| *v = 100.0f32.min(*v + 3.0));
                            }
                            else {
                                volume.with_mut(|v| *v = 0.0f32.max(*v - 3.0));
                            }
                            let p = cx.props.player.write_silent();
                            TrackPlayer::set_volume(p.inner_c(), **volume / 100.0);
                            e.cancel_bubble();
                        }
                    }
                    img { class: "vol-btn",
                        src: "{speaker}",
                    }
                }
            }
            div { class: "scrubber-container",
                p { class: "scrubber-elapsed",
                    "{el_fmt}"
                }
                input { class: "slider scrubber-slider",
                    r#type: "range",
                    min: "0",
                    max: "{du_secs}",
                    value: "{scrubber_value}",
                    onmousedown: move |_| {
                        scrubber_held.set(true);
                        held_value.set(el_secs);
                    },
                    onmouseup: |_| {
                        scrubber_held.set(false);
                    },
                    oninput: move |e| {
                        let val = match e.value.parse::<u64>() {
                            Ok(val) => val,
                            Err(err) => {
                                error!("Failed to parse scrubber value: {}", err);
                                0
                            },
                        };
                        held_value.set(val);
                    },
                    onchange: move |e| {
                        let val = match e.value.parse::<u64>() {
                            Ok(val) => val,
                            Err(err) => {
                                error!("Failed to parse scrubber value: {}", err);
                                0
                            },
                        };
                        let p = cx.props.player.write_silent();
                        if let Err(err) = TrackPlayer::scrub_to_time(p.inner_c(), val) {
                            error!("Scrubbing failed: {}", err);
                        }
                    }
                }
                p { class: "scrubber-remaining",
                    "{du_fmt}"
                }
            }
        }
    })
}
